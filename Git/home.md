
# What is GIT?

<!-- > GO DIGITAL FOR DYNAMIC DATA COLLECTION For more info visit https://fieldon.com/. -->

<p> Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency.<p>


# Version control


<p>Version control is a management system that takes into consideration modifications you’ve made on a file or a set of files (example: a code project). With this system, developers can collaborate and work together on the same project.

A branch system is carried by version control and allow developers to work individually on a task (example: One branch, one task or one branch, one developer) before to combine all changes made by the collaborators into the main branch.

All changes made by developers are traced and saved in a history. It can be beneficial to track modifications made by every collaborator.</p>

<!-- [filename](media/example.md ':include :type=code :fragment=demo') -->
<!-- [filename](_media/example.js ':include :type=code :fragment=demo') -->
<!-- ![logo](media/Capture.PNG) -->

# Git Installation

<p>Now you need to install Git tools on your computer. There are different Git software, but it’s better to install the basic one to start. We will use the command line to communicate with GitLab.</p>


- Download the latest [Git](https://git-scm.com/downloads) and follow instructions on your screen (you can leave the default options).

- Finally, open a terminal and verify that Git is installed correctly:

[filename](media/example.md ':include :type=code :fragment=demo')

![logo](media/checkVersion.png)

- Go to [Gitlab](https://about.gitlab.com/) and create an account.

- One last step is needed to complete the installation correctly! You need to run in your terminal the following commands with your information to set a default username and email when you are going to save your work.

[filename](media/gitConfiguration.md ':include :type=code :fragment=demo')


<hr>

# Basic commands 

> See What Branch You're On

  - Run this command : <code>git status</code>

> List All Branches

  <code>NOTE:</code> The current local branch will be marked with an asterisk (*).

  - To see local branches, run this command: 
    - <code> git branch </code>

  - To see remote branches, run this command: 
    - <code> git branch -r </code>

  - To see all local and remote branches, run this command: 
    - <code> git branch -a </code>

> How to Create a New Branch?

  - Run this command (replacing my-branch-name with whatever name you want):
    - <code>git checkout -b my-branch-name</code>

  - You're now ready to commit to this branch.

> Switch to a Branch In Your Local Repo.
  
  1. To get a list of all branches from the remote, run this command:
    - <code>git pull</code>

  2. Run this command to switch to the branch:
    - <code>git checkout --track origin/my-branch-name</code>

> How to Clone a Repository? 
  
  - To clone a repository, run this command <code>$ git clone [HTTPS ADDRESS]</code>
    - This command will make a local copy of the repository hosted at the given address.

    ![logo](media/cloneImage.PNG)

    - <p>Note: When you clone, Git will create a repository on your computer. If you want, you can access your project with the computer user interface.</p>

> How to add a file.

  - <code>“add”:</code> With the help of the change list, you can add all files you want to upload with the following command:

  - <code> $ git add README.md </code>

   ![logo](media/gitAdd.PNG)

  - <p> <code>Note:</code>If you type again “git status”, the “README.md” will appear now in green. This means that we have added the file correctly.</p>

> How to Commit?

 - <code>“commit”:</code> Now that we have added the files of our choice, we need to write a message to explain what we have done. This message may be useful later if we want to check the change history. Here is an example of what we can put in our case.

 - <code>$ git commit -m "Added README.md with good description in it."</code>

> Push to a Branch

  - If your local branch does not exist on the remote, run either of these commands:

     - <code>git push -u origin my-branch-name</code>

     - <code>git push -u origin HEAD</code>

    <code>NOTE:</code> HEAD is a reference to the top of the current branch, so it's an easy way to push to a branch of the same name on the remote. This saves you from having to type out the exact name of the branch!

  - If your local branch already exists on the remote, run this command:

    - <code>git push</code>

> Useful commands for Git

 - Display the history of commits (all modifications made on the project).
 
  - <code>$ git log</code>

 - Revert back all your changes since the last commit.

  - <code>$ git checkout [FILENAME]</code>

 - Display the last changes on a file since the last commit.

  - <code>$ git diff [FILENAME]</code>

