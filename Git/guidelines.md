# GuideLines 

1. <p>No user should create Magikminds projects with personal email-Ids all the repos usernames & emailId's should be of Magikminds.</p>
  - <code> IMPORTANT:</code> All the project implementaions based of FON should be under one group and the owners and maintainers should create projects for the team and add the members for only project not to the Group. should have track of all projects Repos documentaion maintained in an Excel.

2. Primary ownership for Gitlab has been set to karthik Sagar & Phani Kumar rest can be a choice.

3. Max of 1 to 2 maintainers should be allocated for a project.

4. Freeze your branches to avoid pushing data to wrong branches OWNER & Maintainer should validate it after the sprint is completed and check if the branch is Freezed. 
  -  <code>NOTE :</code> No one is allowed to merge/push once branch is considered as freezed.
        - FOR REFERENCE : Goto projectname/settings/repository -> Protected Branches

5. No GIT access should be provided/links/usercredentials to be shared with other team members/outside the office.

6. Guest member request access should given a certain time period and there should be an formal email from the manager requesting access to the particular member.

7. Use https or SSH only to access git repositories.

8. Configure access rights to your projects.

9. Review access rights to your projects periodically.

10. Deploy code remotely in a safe manner.[Always have at least one copy of repository that you can trust and treat as secure]

11. Perform manual source code review at least for every branch before doing a release. In reality, you may do it more often 

12. Define code owners for faster code reviews.

13. Archive dead repositories - we find ourselves with unmaintained repositories. 
   Sometimes developers create repos for an ad-hoc usecase, a POC, or some other reason. Sometimes they inherit repos with old and irrelevant code. 

14. Use a meaningful branch naming convention.

15. Delete stale branches  
    More info : Every time one branch is merged into another, the branch that it merged in becomes stale,assuming further work isn’t being done in it. saves Repo size.

16. Remove inactive Team Members access for the Repos when they are not working on that project.

17. Maintain ReadmeFile for every branch and plese do update it after the sprint is completed.

18. Provide Proper commit messages for each commit you do.