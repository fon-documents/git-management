# FAQ


### How to Login?

- Go to [Gitlab](https://about.gitlab.com/)

 ![logo](media/gitLogin.PNG)

<hr>

### How do I create a new git repository?

To create a repository, create a directory for the project if it does not exist, then run the command “git init”. By running this command .git directory will be created in the project directory.

<hr>

### What is a repository in Git

Repository in Git is a place where Git stores all the files. Git can store the files either on the local repository or on the remote repository.

<hr>

### What is the difference between ‘git remote’ and ‘git clone’?

‘git remote add’ creates an entry in your git config that specifies a name for a particular URL whereas ‘git clone’ creates a new git repository by copying an existing one located at the URL

<hr>

###  What is the function of ‘git config’?

Git uses your username to associate commits with an identity. The git config command can be used to change your Git configuration, including your username.

`For Example:`

Suppose you want to give a username and email id to associate a commit with an identity so that you can know who has made a particular commit. For that I will use:

<code>git config –global user.name “Your Name"</code> This command will add a username.

<code>git config –global user.email “Your E-mail Address”</code> This command will add an email id. 

<hr>

### How to Create a group

Goto > Groups and click on `New group`

![logo](media/newGroup.PNG)

- You can see this window to create a new Group

![logo](media/newGroupWindow.PNG)

After clicking on `create group` button the group will be created sucessfully.

<hr>

### How to Create Project

To create new project, login to your GitLab account and click on the New project button in the dashboard.

![logo](media/projects.PNG)

 -  select blank project

![logo](media/blank_project.png)

 - It will open the New project screen as shown below in the image −

![logo](media/new_project_window.PNG)

   - Enter the project name, description for the project, visibility level (accessing the project's visibility in publicly or internally) and click on the Create project button.

<hr>

### Permission Levels

The following table shows available permission levels for different types of users

<table>
<thead>
<tr>
<th>#</th>
<th>Guest</th>
<th>Reporter</th>
<th>Developer</th>
<th>Master</th>
</tr>
</thead>
<tbody>
<tr>
<td>1</td>
<td>Creates a new issue</td>
<td>Creates a new issue</td>
<td>Creates a new issue</td>
<td>Creates a new issue</td>
</tr>
<tr>
<td>2</td>
<td>Can leave comments</td>
<td>Can leave comments</td>
<td>Can leave comments</td>
<td>Can leave comments</td>
</tr>
<tr>
<td>3</td>
<td>Able to write on project wall</td>
<td>Able to write on project wall</td>
<td>Able to write on project wall</td>
<td>Able to write on project wall</td>
</tr>
<tr>
<td>4</td>
<td>-</td>
<td>Able to pull project code</td>
<td>Able to pull project code</td>
<td>Able to pull project code</td>
</tr>
<tr>
<td>5</td>
<td>-</td>
<td>Can download project</td>
<td>Can download project</td>
<td>Can download project</td>
</tr>
<tr>
<td>6</td>
<td>-</td>
<td>Able to write code snippets</td>
<td>Able to write code snippets</td>
<td>Able to write code snippets</td>
</tr>
<tr>
<td>7</td>
<td>-</td>
<td>-</td>
<td>Create new merge request</td>
<td>Create new merge request</td>
</tr>
<tr>
<td>8</td>
<td>-</td>
<td>-</td>
<td>Create new branch</td>
<td>Create new branch</td>
</tr>
<tr>
<td>9</td>
<td>-</td>
<td>-</td>
<td>Push and remove non protected branches</td>
<td>Push and remove non protected branches</td>
</tr>
<tr>
<td>10</td>
<td>-</td>
<td>-</td>
<td>Includes tags</td>
<td>Includes tags</td>
</tr>
<tr>
<td>11</td>
<td>-</td>
<td>-</td>
<td>Can create, edit, delete project milestones</td>
<td>Can create, edit, delete project milestones</td>
</tr>
<tr>
<td>12</td>
<td>-</td>
<td>-</td>
<td>Can create or update commit status</td>
<td>Can create or update commit status</td>
</tr>
<tr>
<td>13</td>
<td>-</td>
<td>-</td>
<td>Write a wiki</td>
<td>Write a wiki</td>
</tr>
<tr>
<td>14</td>
<td>-</td>
<td>-</td>
<td>Create new environments</td>
<td>Create new environments</td>
</tr>
<tr>
<td>15</td>
<td>-</td>
<td>-</td>
<td>Cancel and retry the jobs</td>
<td>Cancel and retry the jobs</td>
</tr>
<tr>
<td>16</td>
<td>-</td>
<td>-</td>
<td>Updates and removes the registry image</td>
<td>Updates and removes the registry image</td>
</tr>
<tr>
<td>17</td>
<td>-</td>
<td>-</td>
<td>-</td>
<td>Can add new team members</td>
</tr>
<tr>
<td>18</td>
<td>-</td>
<td>-</td>
<td>-</td>
<td>Push and remove protected branches</td>
</tr>
<tr>
<td>19</td>
<td>-</td>
<td>-</td>
<td>-</td>
<td>Can edit the project</td>
</tr>
<tr>
<td>20</td>
<td>-</td>
<td>-</td>
<td>-</td>
<td>Can manage runners, job triggers and variables</td>
</tr>
<tr>
<td>21</td>
<td>-</td>
<td>-</td>
<td>-</td>
<td>Add deploy keys to project</td>
</tr>
<tr>
<td>22</td>
<td>-</td>
<td>-</td>
<td>-</td>
<td>Able to manage clusters</td>
</tr>
<tr>
<td>23</td>
<td>-</td>
<td>-</td>
<td>-</td>
<td>Configure project hooks</td>
</tr>
<tr>
<td>24</td>
<td>-</td>
<td>-</td>
<td>-</td>
<td>Can enable/disable the branch protection</td>
</tr>
<tr>
<td>25</td>
<td>-</td>
<td>-</td>
<td>-</td>
<td>Able to rewrite or remove Git tags</td>
</tr>
</tbody>
</table>

<hr>

### Create a new branch from a project’s dashboard

If you want to make changes to several files before creating a new merge request, you can create a new branch upfront.

1. From a project’s files page, choose New branch from the dropdown.
    
    ![logo](media/newBranch.PNG)

2. Enter a new Branch name.
3. (Optional) Change the Create from field to choose which branch, tag, or commit SHA this new branch originates from. This field autocompletes if you start typing an existing branch or tag.
4. Click Create branch to return to the file browser on this new branch.
    
    ![logo](media/newBranchwindow.PNG)

You can now make changes to any files, as needed. When you’re ready to merge the changes back to master, you can use the widget at the top of the screen. This widget only appears for a period of time after you create the branch or modify files.

![logo](media/newBranchCreated.PNG)

<hr>

### Protected branches

Permissions in GitLab are fundamentally defined around the idea of having read or write permission to the repository and branches. To impose further restrictions on certain branches, they can be protected.

- **Overview**

 - By default, a protected branch does these things:

  - It prevents its creation, if not already created, from everybody except users with Maintainer permission.
  - It prevents pushes from everybody except users with Allowed permission.
  - It prevents anyone from force pushing to the branch.
  - It prevents anyone from deleting the branch.

- **Permissions**

 - GitLab administrators are allowed to push to the protected branches.
 - Users with Developer permissions are allowed to create a project in a group, but might not be allowed to initially push to the default branch.

<hr>

### Configuring protected branches

To protect a branch, you need to have at least Maintainer permission level. The default branch for your repository is protected by default.

1. In your project, go to Settings > `Repository`.
2. Scroll to find the Protected branches section.
3. From the Branch dropdown menu, select the branch you want to protect and click Protect. In the screenshot below, we chose the `develop` branch.

    ![logo](media/protectBranch.PNG)

4. Once done, the protected branch displays in the Protected branches list.

    ![logo](media/developBranchProtection.PNG)

<hr>

### Using the Allowed to merge and Allowed to push settings

In GitLab 8.11 and later, we have another layer of branch protection which provides more granular management of protected branches. The Developers can push option was replaced by Allowed to push. You can set this value to allow or prohibit Maintainers and/or Developers to push to a protected branch.

Using the `Allowed to push` and `Allowed to merge` settings, you can control the actions that different roles can perform with the protected branch. `For example`, you could set `Allowed to push` to “No one”, and `Allowed to merge` to “Developers + Maintainers”, to require everyone to submit a merge request for changes going into the protected branch. This is compatible with workflows like the GitLab workflow.

However, there are workflows where that is not needed, and only protecting from force pushes and branch removal is useful. For those workflows, you can allow everyone with write access to push to a protected branch by setting `Allowed to push` to “Developers + Maintainers”.

You can set the `Allowed to push` and `Allowed to merge` options while creating a protected branch or afterwards by selecting the option you want from the dropdown list in the `Already protected` area.


![logo](media/already_protected.PNG)

If you don’t choose any of those options while creating a protected branch, they are set to `Maintainers` by default.

<hr>

### Members of a project

You can manage the groups and users and their access levels in all of your projects. You can also personalize the access level you give each user, per-project.

You should have `Maintainer` or `Owner` permissions to add or import a new user to your project.

To view, edit, add, and remove project’s members, go to your project’s Members.

<hr>

### Inherited membership

When your project belongs to the group, group members inherit the membership and permission level for the project from the group.

![logo](media/inheritedMembers.PNG)

From the image above, we can deduce the following things:

- There are 3 members that have access to the project.
- User0 is a Reporter and has inherited their permissions from group demo which contains current project.
- User1 is shown as a `Direct member` in the `Source` column, therefore they belong directly to the project we’re inspecting.
- Administrator is the Owner and member of `all` groups and for that reason, there is an indication of an ancestor group and inherited Owner permissions.

<hr>

### How to Add a user to a Project

Goto > Project > Group members

start typing the name or username of the user you want to add. in the invite member tab

![logo](media/newMember.PNG)

Select the user and the permission level that you’d like to give the user. Note that you can select more than one user.

![logo](media/newMember1.PNG)

Once done, select Add users to project and they are immediately added to your project with the permissions you gave them above.

![logo](media/newMember2.PNG)

From there on, you can either remove an existing user or change their access level to the project.

### Quick Tips

 - **How to solve the issue `The project you were looking for could not be found`.**

  If you come across this error while using GIT try the below command as shown below

  `https://{gitlab_user}@gitlab.com/gitlab_user/project_repo.git`

![logo](media/git_error.png)
  

