# Version History


Version History of Product `Fieldon` and `Fieldon Progress`

### FormszMigration [GROUP]

<details>
<summary>Group Members (Click to expand)</summary>

- <code>Owners</code> : Karthik Panchangam , Phani Kumar Narina

- <code>Maintainer</code>:	Sindhu, renuka

- <code>Developers</code>: NA
 
- <code>Guest</code>: NA

</details>

> FieldON

- Projects: 

  - `formsz3.0-webapplication` :

  - `FON-Mobile` :

  - `Formsz3.0-Servercode-Mobile` :

  - `Formsz3.0-Servercode-Web` :

  - `Formsz3.0-Mobile-iOS` :

  > FieldON-Progress:

    - `FON-Progress-Web` :

        - https://gitlab.com/formszmigration/fon-progress-web.git

        - Branches Available : `Sprint1`

        - Members : `Karthik [**OWNER**]` `Phani Kumar Narina [**OWNER**]` `Sindhu [Maintainer]` 
        `renuka [Maintainer]` `Akhil Gangula [Developer]` `Javeria [Developer]` `Prakash Bhukya [Developer]` `divya bandaru [Developer]` 

    - `FON-Progress-Server`:
       
       - https://gitlab.com/formszmigration/fon-progress-server.git

        - Branches Available : `Sprint1`

        - Members : `Karthik [**OWNER**]` `Phani Kumar Narina [**OWNER**]` `Sindhu [Maintainer]` 
        `renuka [Maintainer]` `Akhil Gangula [Developer]` `Javeria [Developer]` `Prakash Bhukya [Developer]` `divya bandaru [Developer]`

    - `FON-Progress-Mobile` :

       - https://gitlab.com/formszmigration/fon-progress-mobile.git

       - Branches Available : `Sprint1`

       - Members : `Karthik [**OWNER**]` `Phani Kumar Narina [**OWNER**]` `Sindhu [Maintainer]` 
        `renuka [Maintainer]` `Akhil Gangula [Developer]` `Javeria [Developer]` `Prakash Bhukya [Developer]` `divya bandaru [Developer]`


<!-- <hr> 

### ThinkGas-Implementations

<details>
<summary>Group Members (Click to expand)</summary>

- <code>Owners</code> : Karthik Panchangam , Sindhu

- <code>Maintainer</code>:	Phani Kumar Narina

- <code>Developers</code>: Akhil Gangula, Gouri Shankar Karanam, ManeeshaTalla, Srinvas Rachamalla, divya bandaru, renuka
 
- <code>Guest</code>: NA

</details>


- Sub-projects

<table>
    <thead>
        <tr>
            <th>#</th>
            <th>Project Name</th>
            <th>Platform</th>
            <th>URL</th>
        </tr>
    </thead>
    <tbody>
    <tr>
        <td>1</td>
        <td>Web-Application-TG</td>
        <td><code>WEB</code></td>
        <td>https://gitlab.com/thinkgas-implementations/web-application-tg.git</td>
    </tr>
    <tr>
        <td>2</td>
        <td>Mobile Application</td>
        <td><code>Mobile</code></td>
        <td>https://gitlab.com/thinkgas-implementations/mobile-application.git</td>
    </tr>
     <tr>
        <td>3</td>
        <td>Server</td>
        <td><code>Server</code></td>
        <td>https://gitlab.com/thinkgas-implementations/server.git</td>
    </tr>
    </tbody>
</table>

<p class="tip">NOTE: Clone the Respective Branch by using command git clone -b "branchname" URL</p> -->