# Git Architecture.

- Refer this [Architecture Diagram](https://www.figma.com/proto/C3ijQkAyzeucI0E0FB4Zwx/GIT-ARCHITECTURE?node-id=104%3A77&scaling=min-zoom) how to perform Git Maintanance.

![logo](media/architecture.png)

> Development Cycle

1. <p>All the Development starts with an <code>MASTER branch</code> which holds the main source code. </p>
   
   - <code>NOTE:</code> This is a protected Branch and no one is allowed to push to this Branch with out the confirmation from the Respective Manager.

2. Developers will work on Development Branch taken from Master copy. and will work on Sprint-1

3. The Development of Sprint-1 will start and once the development is completed testing is performed. and when build is stable then the <code>Sprint-1</code> is merged to <code>Development Branch</code>

4. This cycle repeats till the development branches holds the code of <code>Sprint1- SprintN.</code>

5. Once The development Branch is stable we need to Merge the <code>Development Branch (Source) to Master (Destination)</code>

![logo](media/devtomaster.PNG)

6. **Protected branches**

Keep stable branches secure, and force developers to use merge requests.

By default, protected branches protect your code and:

- Allow only users with Maintainer permissions to create new protected branches.
- Allow only users with Maintainer permissions to push code.
- Prevent anyone from force-pushing to the branch.
- Prevent anyone from deleting the branch.

![logo](media/protectbranches.PNG)

- You can setup the rules for each branch how can Push/Merge

> Customer Releases

1. All the customer Releases will be derived from MASTER Copy

> Maintenance & Support Activity